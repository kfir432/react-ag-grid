import React, {Component} from 'react';
import './App.css';
import AgGridTable from './AgGridTable/AgGridTable';
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from '@material-ui/core/Tab';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: 0,
            totalRows: 0,
            tableTotalRowsToShow: 50,
            currentPage: 0,
            dataVirtualized: [],
            dataTableMap: new Map(),
            dataVirtualizedLoading: false,
            tableSearchValue: '',
            rows: [],
            columns: [],
        };
        this.dataMap = new Map();
    }

    addRowCounter(data, startIndex = 0) {
        return data.map((row, index) => {
            row.row = index + 1;
            row.row = (startIndex ? startIndex++ : index + 1);
            return row;
        });
    }

    buildDataFromFirstResult() {
        //clear data
        let rowData = [];
        const headers = [];

        fetch('https://api.myjson.com/bins/15psn9')
            .then(result => result.json())
            .then(rows => rowData = rows).then(() => {
           // headers.push({headerName: 'row', field: 'row', type: 'INTEGER'});
            let headersObjKeys = Object.keys(rowData[0]);
            headersObjKeys.map(header => {
                headers.push({headerName: header, field: header});
            });
            this.setState({rows: rowData, columns: headers});
        });


        // this.setState({headers: [], data: []});
        // this.dataMap = new Map();
        // const headers = [];
        // const data = firstResult && firstResult.rows ? this.addRowCounter(firstResult.rows) : [];
        // headers.push({field: 'row', title: 'row', type: 'INTEGER'});
        // for (let i = 0; i < firstResult.schema.length; i++) {
        //     headers.push(
        //         {
        //             headerName: firstResult.schema[i].name,//utils.camelCaseToRegular(firstResult.schema[i].name),
        //             field: firstResult.schema[i].name,
        //             title: firstResult.schema[i].name,
        //             type: firstResult.schema[i].type,
        //             maxCharsLength: firstResult.schema[i].maxCharsLength
        //         }
        //     );
        // }

        // this.setState({headers, data}, () => {
        //     this.splitDataToPages(data, this.state.tableTotalRowsToShow, true);
        // });
    }

    splitDataToPages(data, totalRowsToPage, setVirtualizeData = false, currentPage = 0) {
        let chunkArray = this.chunkArray(data, totalRowsToPage);
        //console.log(chunkArray);
        let dataMap = this.dataMap;
        //let dataMap = new Map();
        let currentPageTemp = currentPage;
        chunkArray.map((arr, index) => {
            let pageNumber = currentPageTemp > 0 ? currentPageTemp++ : index;
            dataMap.set(pageNumber, arr);
        });
        //console.log('dataMap', dataMap);
        // console.log('currentPage', currentPage);
        if (setVirtualizeData) {
            this.setState({dataTableMap: dataMap}, () => {
                this.currentPageData(currentPage);
            });
        } else {
            this.setState({dataTableMap: dataMap});
        }
    }

    chunkArray(myArray, chunk_size) {
        let index = 0;
        let arrayLength = myArray.length;
        let tempArray = [];
        for (index = 0; index < arrayLength; index += chunk_size) {
            let myChunk = myArray.slice(index, index + chunk_size);
            // Do something if you want with the group
            tempArray.push(myChunk);
        }
        return tempArray;
    }

    getDataFromApi(currentPage, maxResults, startIndex) {
        this.setState({dataVirtualizedLoading: true});
        const {widgetData} = this.props;
        const {tableTotalRowsToShow, data} = this.state;
        let widgetId = widgetData ? widgetData.i : null;
        let dashboardId = widgetData && widgetData.data ? widgetData.data.query.dashboardId : null;
        let newRows = [];

        console.error('kfir need to fix me');
        // this.props.ResultsStore.getFirstResultRows(widgetId, startIndex, maxResults, dashboardId).then(rows => {
        //     let rowNumberStartIndex = tableTotalRowsToShow * currentPage + 1;
        //     newRows = this.addRowCounter(rows, rowNumberStartIndex);
        //     this.setState({data: [...data, ...newRows], dataVirtualizedLoading: false}, () => {
        //         this.splitDataToPages(newRows, tableTotalRowsToShow, true, currentPage);
        //     });
        // }).catch(err => {
        //     this.setState({dataVirtualizedLoading: false});
        //     console.log('err', err);
        // });
        //console.log(newRows);
    }

    currentPageData(currentPage = 0) {
        //at page zero we already have 200 rows of data
        //start from page 1 get 200 rows from api and add them to the data
        const {tableTotalRowsToShow, dataTableMap, dataVirtualizedLoading} = this.state;
        const MAX_RESULT = 200;
        let startIndex = currentPage * tableTotalRowsToShow;
        let endIndex = (currentPage * tableTotalRowsToShow) + tableTotalRowsToShow;
        // let dataVirtualized = [];
        console.log('startIndex', startIndex);
        //  console.log('endIndex', endIndex);
        //if page of rows data exist take from state else get from api
        //console.log(dataTableMap);
        console.log('currentPage', currentPage);
        if (dataTableMap.has(currentPage)) {
            let dataVirtualized = dataTableMap.get(currentPage);
            this.setState({dataVirtualized});
        } else {
            if (!dataVirtualizedLoading) {
                console.log('go to api');
                this.getDataFromApi(currentPage, MAX_RESULT, startIndex);
            }
        }
    }

    changePageSize(pageSize) {
        const {currentPage, data} = this.state;
        let startIndex = currentPage * pageSize;
        let endIndex = (currentPage * pageSize) + pageSize;
        let dataVirtualized = this.virtualizeData(startIndex, endIndex, data);
        this.setState({dataVirtualized, tableTotalRowsToShow: pageSize});
    }

    searchChange(value) {
        this.setState({tableSearchValue: value});
    }

    handleChange = (event, value) => {
        this.setState({value});
    };

    componentDidMount() {
        this.buildDataFromFirstResult();
    }

    render() {
        const {value, columns, rows} = this.state;

        return (
            <div>
                <AppBar position="static">
                    <Tabs value={value} onChange={this.handleChange}>
                        <Tab label="Grid One"/>
                        <Tab label="Grid Two"/>
                        <Tab label="Grid Three"/>
                    </Tabs>
                </AppBar>
                {value === 0 && <div><AgGridTable rowData={rows} columnDefs={columns}/></div>}
                {value === 1 && <div>Item Two</div>}
                {value === 2 && <div>Item Three</div>}
            </div>
        );
    }
}

export default App;