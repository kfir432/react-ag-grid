import React, {Component} from 'react';

import {AgGridReact} from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import _ from 'lodash';

class AgGridTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            columnDefs: [],
            rowData: []
        };
        this.gridApi = {};
        this.gridColumnApi = {};
        this.onGridReady = this.onGridReady.bind();
    }

    componentWillMount() {
        this.setData();
    }

    isEquivalent(a, b) {
        // Create arrays of property names
        var aProps = Object.getOwnPropertyNames(a);
        var bProps = Object.getOwnPropertyNames(b);

        // If number of properties is different,
        // objects are not equivalent
        if (aProps.length != bProps.length) {
            return false;
        }

        for (var i = 0; i < aProps.length; i++) {
            var propName = aProps[i];

            // If values of same property are not equal,
            // objects are not equivalent
            if (a[propName] !== b[propName]) {
                return false;
            }
        }

        // If we made it this far, objects
        // are considered equivalent
        return true;
    }

    componentDidUpdate(nextProps) {
        if (!this.isEquivalent(nextProps.rowData, this.props.rowData)) {
            this.setData();
        }
    }

    setData() {
        console.log('set data');
        const {rowData = [], columnDefs = []} = this.props;
        this.setState({rowData, columnDefs});
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }
    onPaginationChanged(page){
        console.log(this.gridApi);
    }

    render() {
        const {sorting = false} = this.props;
        const {rowData, columnDefs, pageSize = 50} = this.state;
        return (
            <div
                className="ag-theme-balham"
                style={{
                    height: '500px',
                    width: '600px'
                }}
            >
                <AgGridReact
                    columnDefs={columnDefs}
                    rowData={rowData}
                    enableSorting={sorting}
                    enableFilter={true}
                    pagination={true}
                    paginationPageSize={pageSize}
                    onGridReady={this.onGridReady}
                    onPaginationChanged={this.onPaginationChanged.bind(this)}
                >
                </AgGridReact>
            </div>
        );
    }
}

export default AgGridTable;